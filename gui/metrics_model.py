import matplotlib
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
import numpy as np
from tensorflow import keras, config, debugging
import tensorflow.keras.backend as K
import nibabel as nib
from os import listdir
import math
import pandas as pd
from scipy import stats
from scipy import ndimage
from sklearn.cluster import AgglomerativeClustering
import os #LTM

# This file contains functions which will perform the disc and/or vertebra segmentation and create
# predictions masks of the specific regions in the spine. It also has functions to create figures
# of the masks for the various slices.

os.environ["CUDA_VISIBLE_DEVICES"] = "-1" #LTM
gpu = config.list_physical_devices('GPU') #LTM

## functions
def read_nifti_file(filepath):
    scan = nib.load(filepath)
    scan = scan.get_fdata()
    return np.moveaxis(scan, -1, 0)


def standardize_scan(X):
    return (X - X.mean()) / X.std()


def get_vois(filedir, identify_discs=False):
    vois_files = list(filter(lambda x: "discT2W.nii" in x, listdir(filedir)))
    
    if identify_discs:
        vois_files.sort()
    
    vois_images = list(map(lambda x: read_nifti_file(f"{filedir}/{x}"), vois_files))
   
    if identify_discs:
        return np.add.reduce(
            [clustered_disc * (i + 1) for i, clustered_disc in enumerate(vois_images)]
        ).astype(int)
    return np.any(vois_images, axis=0).astype(int)


def cluster_segments(
    sample,
    custom_distance_threshold_fn=None,
    apply_cluster_transformation=None,
    y_weight=4,
    cluster_labels=None,
):
    x, y = sample.nonzero()
    data = np.array([x, y * y_weight]).T
    if len(data) == 0:
        return {}

    if custom_distance_threshold_fn is not None:
        distance_threshold = custom_distance_threshold_fn(data)
    else:
        distance_threshold = 1000

    clusters = AgglomerativeClustering(n_clusters=None, 
                                       distance_threshold=distance_threshold, 
                                       linkage='single').fit(data) 
                                       # linkage{‘ward’, ‘complete’, ‘average’, ‘single’}, default=’ward’
    labels = clusters.labels_

    cluster_df = pd.DataFrame(np.array([y, x, labels]).T, columns=["x", "y", "cluster"])
    cluster_df = cluster_df[
        (np.abs(stats.zscore(cluster_df[["x", "y"]])) < 4).all(axis=1)
    ]

    return group_clusters_return_matrix(
        cluster_df,
        apply_cluster_transformation,
        cluster_labels=cluster_labels,
        y_weight=y_weight,
    )


def cluster_segments_scan(
    vois,
    apply_cluster_transformation=None,
    custom_distance_threshold_fn=None,
    y_weight=4,
    cluster_labels=["L5S1", "L4L5", "L3L4", "L2L3", "L1L2"],
):
    segmented_scans = []

    centroids_db = {label: [0, 0] for label in cluster_labels}
    n_of_clusters = 0

    for scan_voi in vois.squeeze():
        clustered_scan = cluster_segments(
            scan_voi,
            apply_cluster_transformation=apply_cluster_transformation,
            custom_distance_threshold_fn=custom_distance_threshold_fn,
            y_weight=y_weight,
            cluster_labels=cluster_labels,
        )
        clustered_scan_items = list(clustered_scan.items())
        segmented_scans.append(clustered_scan_items)
        if len(clustered_scan_items) == len(cluster_labels):
            n_of_clusters += 1
            for cluster, scan_items in clustered_scan_items:
                centroids_db[cluster][0] += scan_items["centroid"][0]
                centroids_db[cluster][1] += scan_items["centroid"][1]

    if n_of_clusters != 0:
        for disc, _ in centroids_db.items():
            centroids_db[disc][0] /= n_of_clusters
            centroids_db[disc][1] /= n_of_clusters

    segmented_scans = [
        label_clusters_from_centroids(
            clustered_scan, centroids_db, n_labels=len(cluster_labels)
        )
        for clustered_scan in segmented_scans
    ]
    return list(
        map(
            lambda x: [
                [cluster, cluster_body["matrix"]] for cluster, cluster_body in x
            ],
            segmented_scans,
        )
    )


def group_clusters_return_matrix(
    cluster_df, apply_cluster_transformation=None, cluster_labels=None, y_weight=1
):
    result = {}

    grouped_df = cluster_df.groupby("cluster")

    for cluster, cluster_df_body in grouped_df:
        centroid = cluster_df_body[["x", "y"]].mean()
        cluster_matrix = np.zeros((512, 512))
        cluster_matrix[cluster_df_body["y"], cluster_df_body["x"]] = 1
        if apply_cluster_transformation is not None:
            cluster_matrix = apply_cluster_transformation(cluster_matrix)

        result[cluster] = {
            "centroid": [centroid["x"], centroid["y"] * y_weight],
            "matrix": cluster_matrix,
        }

    if cluster_labels is not None and len(result) == len(cluster_labels):
        clustered_res = {}
        sorted_result = sorted(
            result.items(), key=lambda x: x[1]["centroid"], reverse=True
        )
        for [cluster, (_, body)] in zip(cluster_labels, sorted_result):
            clustered_res[cluster] = body
        return clustered_res

    return result


def label_clusters_from_centroids(clusters, centroids, n_labels):
    if len(clusters) == n_labels:
        return clusters
    res = []
    for cluster, body in clusters:
        smallest_centroid = math.inf
        new_cluster = cluster
        curr_centroid = body["centroid"]
        for centroid_cluster, centroid in centroids.items():
            dist = l2_dist(curr_centroid, centroid)
            if dist < smallest_centroid:
                new_cluster = centroid_cluster
                smallest_centroid = dist
        res.append([new_cluster, body])
    return res


def transfer_img(img):
    img = ndimage.rotate(img, 90, reshape=False)
    return np.flip(img)


def l2_dist(x, y):
    return math.sqrt((x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)


## clustering
cluster_colors = {
    "L1": "orange",
    "L2": "yellow",
    "L3": "blue",
    "L4": "green",
    "L5": "red",
    "undefined": "pink",
}

cluster_colors_disc = {
    "L1L2": "cyan",
    "L2L3": "orange",
    "L3L4": "red",
    "L4L5": "blue",
    "L5S1": "green",
    "undefined": "pink",
}

matplotlib_cluster_colors = {
    k: matplotlib.colors.ListedColormap(["none", v]) for k, v in cluster_colors.items()
}

matplotlib_cluster_colors_disc = {
    k: matplotlib.colors.ListedColormap(["none", v]) for k, v in cluster_colors_disc.items()
}

###################################################################################################
# Apply the disc prediction model to a given subject
###################################################################################################
def disc_model_run(file_path, vois_path, disc_model):
    """
    Apply the disc model to a subject.
    """
    # get test file
    test_file = file_path
    test_image = read_nifti_file(test_file)

    # Input images are normalized. So, need to normalize any image for testing
    test_image = standardize_scan(test_image)
    test_tensor = K.constant(test_image.reshape(*test_image.shape, 1))

    # with the multiclass, it expects an initial estimate of full set of disc masks
    mask_image = get_vois(vois_path, identify_discs=True)
    mask_tensor = K.constant(mask_image.reshape(*test_image.shape, 1))

    test_tensor = K.constant(np.stack((test_image, mask_image), axis = 3))

    # predict disc
    disc_y_pred = disc_model.predict(test_tensor)
    disc_y_pred = (disc_y_pred > 0.5).astype(int)

    return disc_y_pred, test_image

###################################################################################################
# Apply the vertebra prediction model to a given subject
###################################################################################################
def vertebra_model_run(file_path, vertebra_model):
    """
    Apply the vertebra model to a subject.
    """
    # get test file
    test_file = file_path
    test_image = read_nifti_file(test_file)

    # Input images are normalized. So, need to normalize any image for testing
    test_image = standardize_scan(test_image)
    test_tensor = K.constant(test_image.reshape(*test_image.shape, 1))
    
    # predict vertebra
    vertebra_y_pred = vertebra_model.predict(test_tensor)
    vertebra_y_pred = (vertebra_y_pred > 0.5).astype(int)

    return vertebra_y_pred, test_tensor

###################################################################################################
# Create figures for the prediction masks in the disc.
###################################################################################################
def create_disc_image_masks(disc_y_pred, disc_test_image):
    n_slices = disc_test_image.shape[0]
    
    # plot disc clusters
    figures = []
    clusters = ['L1L2', 'L2L3', 'L3L4', 'L4L5', 'L5S1']
    for m in range(n_slices):
        fig = Figure(figsize=(12, 4), constrained_layout=True)
        fig.suptitle(f'T2 Weighted Scan Slice {m+1} with Clustered Disc Segmentation', fontsize=12)
        ax = fig.add_subplot(1, 1, 1)
        ax.imshow(np.rot90(disc_test_image[m,:,:],-1),cmap="gray", vmin=-1.0, vmax=1.5) 
        for n,cluster in zip(range(1,6), clusters):
            # note that n multi-class segmentation, channel 0 is background and discs are channels 1-5
            ax.imshow(np.rot90(disc_y_pred[m,:,:,n],-1), cmap=matplotlib_cluster_colors_disc[cluster], alpha=0.5, label=cluster)
        patches = [matplotlib.patches.Patch(color=color, label=cluster) for cluster, color in cluster_colors_disc.items()]
        fig.legend(handles=patches, loc='center right')
        figures.append(fig)
    return figures

###################################################################################################
# Create figures for the prediction masks in the vertebra.
###################################################################################################
def create_vertebra_image_masks(vertebra_y_pred, test_tensor):
    clustered_vois = cluster_segments_scan(vertebra_y_pred, 
                                                      custom_distance_threshold_fn=lambda _: 10,
                                                      cluster_labels=["L5", "L4", "L3", "L2", "L1"], 
                                                      y_weight=2)
    figures = []
    for m in range(16):
        fig = Figure(figsize=(12, 4), constrained_layout=True)
        fig.suptitle(f'T2 Weighted Scan Slice {m+1} with Clustered Vertebra Segmentation', fontsize=12)
        ax = fig.add_subplot(1, 1, 1)
        ax.imshow(np.rot90(test_tensor[m], -1), cmap="gray", vmin=-1, vmax=1)
        for [cluster, cluster_vois] in clustered_vois[m]:
            vois_img = transfer_img(cluster_vois)
            if cluster not in matplotlib_cluster_colors:
                cluster = 'undefined'
            ax.imshow(vois_img, cmap=matplotlib_cluster_colors[cluster], alpha=0.5, label=cluster)
        ax.axis('off')
        patches = [matplotlib.patches.Patch(color=color, label=cluster) for cluster, color in cluster_colors.items()]
        fig.legend(handles=patches, loc='center right')
        figures.append(fig)
    return figures
