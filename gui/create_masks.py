import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
import nibabel as nib
import numpy as np
import cv2

# This will open a Nifti File and allow the user to draw masks onto each slice of the file.
# The masks will be saved into a 3D numpy array.
class CreateMasks:
    ###############################################################################################
    # Initialize Variables and Setup.
    ###############################################################################################
    def __init__(self, master, filename, button_names):
        self.master = master

        # the current slice index
        self.current_fig_index = 0

        # the current color
        self.curr_color = "white"
        self.numpy_value = 0

        # button names
        self.button_names = button_names

        # get the nifti file/data and scale it
        self.filename = filename
        img = nib.load(self.filename)
        data = img.get_fdata()
        data = np.moveaxis(data, -1, 0)
        self.data = (data - data.mean()) / data.std()

        # the total number of slices in the nifti file
        self.n_slices = self.data.shape[0]

        # a dictionary to store the masks created on the canvas
        self.canvas_data = {}

        # create the figures
        self.figures = []
        self.axes = []
        for i in range(self.n_slices):
            fig = Figure(figsize=(12, 4), constrained_layout=True)
            fig.suptitle(f'T2 Weighted Scan Slice {i + 1}', fontsize=12)
            ax = fig.add_subplot(1,1,1)
            ax.imshow(np.rot90(self.data[i,:,:], -1), cmap='gray', vmin=-1.0, vmax=1.5)
            self.figures.append(fig)
            self.axes.append(ax)
            self.canvas_data[i] = np.zeros((self.data.shape[1], self.data.shape[2]), dtype=np.uint8)

        # create a new canvas at the first slice
        self.canvas = FigureCanvasTkAgg(self.figures[self.current_fig_index], self.master)
        self.canvas.get_tk_widget().config(width=self.data.shape[1], height=self.data.shape[2])
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # Add a navigation toolbar for the figures
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.master)
        self.toolbar.update()
        self.toolbar.pack(side=tk.BOTTOM, fill=tk.X)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # add functionality to the canvas to allow for drawing to create masks
        self.canvas.mpl_connect('button_press_event', self.startDraw)
        self.canvas.mpl_connect('motion_notify_event', self.draw)
        self.canvas.mpl_connect('button_release_event', self.endDraw)
        self.canvas.mpl_connect('button_press_event', self.fillShape)
        self.canvas.mpl_connect('button_press_event', self.onClick)

        # the x and y coordates of the current mouse location
        self.x, self.y = None, None
        self.shape_coords = []

        # button to quit application
        self.exit_button = tk.Button(master=self.master, text='Exit', command=self.exitApp)
        self.exit_button.pack(side=tk.RIGHT)

        # button to export the masks to a csv file
        self.export_mask_button = tk.Button(master=self.master, text='Export Masks', command=self.exportMasks)
        self.export_mask_button.pack(side=tk.RIGHT)

        # clear the canvas
        self.clear_canvas_button = tk.Button(master=self.master, text='Clear Canvas', command=self.clearCanvas)
        self.clear_canvas_button.pack(side=tk.RIGHT)

        # button to go to the next slice
        self.next_button = tk.Button(master=self.master, text="Next Slice", command=self.showNextSlice)
        self.next_button.pack(side=tk.RIGHT)

        # button to go back to the previous slice
        self.back_button = tk.Button(master=self.master, text="Previous Slice", command=self.showPreviousSlice)
        self.back_button.pack(side=tk.RIGHT)

        # buttons to change between discs/vertebra 
        self.button1 = tk.Button(master=self.master, text=self.button_names[0], command=self.changeColor1)
        self.button1.pack(side=tk.LEFT)
        self.button2 = tk.Button(master=self.master, text=self.button_names[1], command=self.changeColor2)
        self.button2.pack(side=tk.LEFT)
        self.button3 = tk.Button(master=self.master, text=self.button_names[2], command=self.changeColor3)
        self.button3.pack(side=tk.LEFT)
        self.button4 = tk.Button(master=self.master, text=self.button_names[3], command=self.changeColor4)
        self.button4.pack(side=tk.LEFT)
        self.button5 = tk.Button(master=self.master, text=self.button_names[4], command=self.changeColor5)
        self.button5.pack(side=tk.LEFT)


    ###############################################################################################
    # Change the colors of the drawing.
    ###############################################################################################
    def changeColor1(self):
        self.curr_color = "red"
        self.numpy_value = 1

    
    def changeColor2(self):
        self.curr_color = "orange"
        self.numpy_value = 2


    def changeColor3(self):
        self.curr_color = "yellow"
        self.numpy_value = 3


    def changeColor4(self):
        self.curr_color = "green"
        self.numpy_value = 4


    def changeColor5(self):
        self.curr_color = "blue"
        self.numpy_value = 5

    ###############################################################################################
    # Will save the current masks to a binary file in numpy.
    ###############################################################################################
    def exportMasks(self):
        data = self.getCanvasData()
        if self.button_names[0] == 'L1L2':
            filename = 'disc_mask_creations.npy'
        else:
            filename = 'vertebra_mask_creations.npy'
        np.save(filename, data)


    ###############################################################################################
    # Clear the entire canvas.
    ###############################################################################################
    def clearCanvas(self):
        self.canvas.get_tk_widget().delete("all")


    ###############################################################################################
    # When right click on the line, the shape will be filled.
    ###############################################################################################
    def onClick(self, event):
        if self.toolbar.mode != 'zoom rect':
            if event.button == 3:
                self.fillShape(event)
            if event.button == 2:
                print('zoom change')
                self.zoom_on = not self.zoom_on


    ###############################################################################################
    # When the left click is pressed, the drawing will start.
    ###############################################################################################
    def startDraw(self, event):
        if self.toolbar.mode != 'zoom rect':
            if event.button == 1:
                self.x, self.y = event.xdata, event.ydata


    ###############################################################################################
    # While the left click is pressed down, a line will be drawn over the path of the mouse.
    # The canvas data location will be updated as the line is drawn.
    ###############################################################################################
    def draw(self, event):
        if self.toolbar.mode != 'zoom rect':
            if self.x and self.y and event.button == 1:
                self.axes[self.current_fig_index].plot([self.x, event.xdata], [self.y, event.ydata], color=self.curr_color)
                self.canvas.draw()
                self.shape_coords.append((event.xdata, event.ydata))
                self.canvas_data[self.current_fig_index][int(event.ydata):int(event.ydata)+1, int(self.x):int(event.xdata)+1] = self.numpy_value
                self.canvas_data[self.current_fig_index][int(self.y):int(event.ydata)+1, int(self.x):int(event.xdata)+1] = self.numpy_value
            self.x, self.y = event.xdata, event.ydata


    ###############################################################################################
    # End the drawing when the left click is unpressed.
    ###############################################################################################
    def endDraw(self, event):
        if self.toolbar.mode != 'zoom rect':
            if event.button == 1:
                self.x, self.y = None, None


    ###############################################################################################
    # When the user right clicks, the shape drawn will be filled to create a mask.
    ###############################################################################################
    def fillShape(self, event):
        if self.toolbar.mode != 'zoom rect':
            if event.button == 3 and self.shape_coords:
                self.axes[self.current_fig_index].fill(np.array(self.shape_coords)[:,0], np.array(self.shape_coords)[:,1], facecolor=self.curr_color)
                self.canvas.draw()
                shape_mask = np.zeros((self.data.shape[1], self.data.shape[2]), dtype=np.uint8)
                shape_points = np.array(self.shape_coords, dtype=np.int32)
                cv2.fillPoly(shape_mask, [shape_points], color=self.numpy_value)
                self.canvas_data[self.current_fig_index][shape_mask == self.numpy_value] = self.numpy_value
                self.shape_coords = []


    ###############################################################################################
    # Return a 3D numpy array of the canvas data masks that were created.
    ###############################################################################################
    def getCanvasData(self):
        height, width = self.canvas_data[next(iter(self.canvas_data))].shape
        n = len(self.canvas_data)
        canvas_data_3d = np.zeros((height, width, n), dtype=np.float32)
        for i, key in enumerate(sorted(self.canvas_data)):
            canvas_data_3d[:, :, i] = self.canvas_data[key]
        return canvas_data_3d

    
    ###############################################################################################
    # Switch to the next slice.
    ###############################################################################################
    def showNextSlice(self):
        self.toolbar.zoom() # Reset the zoom
        if self.current_fig_index < len(self.figures) - 1:
            self.current_fig_index += 1
            self.canvas._tkcanvas.pack_forget() # Remove the old canvas from the window
            self.packForget()
            self.canvas = FigureCanvasTkAgg(self.figures[self.current_fig_index], master=self.master) # Create a new canvas with the new figure
            self.reinitializeCanvas()


    ###############################################################################################
    # Switch to the previous slice.
    ###############################################################################################
    def showPreviousSlice(self):
        self.toolbar.zoom() # Reset the zoom
        if self.current_fig_index > 0:
            self.current_fig_index -= 1
            self.canvas.figure = self.figures[self.current_fig_index]
            self.canvas._tkcanvas.pack_forget() # Remove the old canvas from the window
            self.packForget()
            self.canvas = FigureCanvasTkAgg(self.figures[self.current_fig_index], master=self.master) # Create a new canvas with the new figure
            self.reinitializeCanvas()


    ###############################################################################################
    # Exit the application.
    ###############################################################################################
    def exitApp(self):
        self.master.destroy()


    ###############################################################################################
    # Reset the buttons.
    ###############################################################################################
    def packForget(self):
        self.exit_button.pack_forget()
        self.export_mask_button.pack_forget()
        self.clear_canvas_button.pack_forget()
        self.next_button.pack_forget()
        self.back_button.pack_forget()
        self.button1.pack_forget()
        self.button2.pack_forget()
        self.button3.pack_forget()
        self.button4.pack_forget()
        self.button5.pack_forget()


    ###############################################################################################
    # Reinitialize the canvas and toolbar.
    ###############################################################################################
    def reinitializeCanvas(self):
        self.canvas.get_tk_widget().config(width=self.data.shape[1], height=self.data.shape[2])
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        if self.toolbar is not None:
            self.toolbar.pack_forget() # Remove the old toolbar from the window
        # Add a navigation toolbar for the figures
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.master)
        self.toolbar.update()
        #self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        self.toolbar.pack(side=tk.BOTTOM, fill=tk.X)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        self.canvas.mpl_connect('button_press_event', self.startDraw)
        self.canvas.mpl_connect('motion_notify_event', self.draw)
        self.canvas.mpl_connect('button_release_event', self.endDraw)
        self.canvas.mpl_connect('button_press_event', self.fillShape)
        self.canvas.mpl_connect('button_press_event', self.onClick)

        self.exit_button.pack(side=tk.RIGHT)
        self.export_mask_button.pack(side=tk.RIGHT)
        self.clear_canvas_button.pack(side=tk.RIGHT)
        self.next_button.pack(side=tk.RIGHT)
        self.back_button.pack(side=tk.RIGHT)

        self.button1.pack(side=tk.LEFT)
        self.button2.pack(side=tk.LEFT)
        self.button3.pack(side=tk.LEFT)
        self.button4.pack(side=tk.LEFT)
        self.button5.pack(side=tk.LEFT)
