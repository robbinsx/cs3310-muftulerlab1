import nibabel as nib
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import csv
from tkinter import ttk
import numpy as np
from metrics_model import disc_model_run, vertebra_model_run, create_disc_image_masks, create_vertebra_image_masks
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import keras
import prediction_masks_viewer as prediction_masks_viewer
import create_masks as create_masks
from matplotlib.figure import Figure


# This class is the driver for the spinal abnormalities MRI analysis. Users will be able to import T1 and T2 weighted
# MR images and a vois folder into the application where various metrics will be calculated.
# TODO: Add more description on what the app can do after completion of GUI
class QuantitativeMRIMetrics:
    ###############################################################################################
    # Initialize Variables and Setup
    ###############################################################################################
    def __init__(self, master):
        self.master = master # Note: master = root
        self.master.title("MRI Spinal Analysis")
        self.master.geometry("1300x750")
        #self.master.resizable(False, False) # if you want to make the GUI a fixed size, uncomment this

        ### prediction models for disc and vertebra masks ###
        disc_h5_file = 'models/unet-discs_multiclass.h5'
        self.disc_model = keras.models.load_model(disc_h5_file)
        vertebra_h5_file = 'models/unet-augmented-512new.h5'
        self.vertebra_model = keras.models.load_model(vertebra_h5_file)

        ### names for the buttons in the create masks viewer ###
        self.disc_names = ['L1L2', 'L2L3', 'L3L4', 'L4L5', 'L5S1']
        self.vertebra_names = ['L1', 'L2', 'L3', 'L4', 'L5']

        ### prediction figures ###
        self.disc_figures = None
        self.vertebra_figures = None

        ### imported filepaths ###
        self.t1_filepath = None
        self.t2_filepath = None
        self.vois_filepath = None

        ### predictions/tensors for masks ###
        self.disc_y_pred = None
        self.disc_test_image = None
        self.vertebra_y_pred = None
        self.vertebra_test_tensor = None

        ### labels/photos ###
        self.t1_img_label = tk.Label(self.master)
        self.t2_img_label = tk.Label(self.master)

        self.t1_photo = None
        self.t1_image_data = None
        self.t2_photo = None
        self.t2_image_data = None

        t1_label = tk.Label(self.master, text="T1 MRI", font=("Helvetica", 12, "bold"),
                            bg="#f0f0f0", padx=5, pady=2.5, relief="solid",width=18)
        t2_label = tk.Label(self.master, text="T2 MRI", font=("Helvetica", 12, "bold"),
                            bg="#f0f0f0", padx=5, pady=2.5, relief="solid", width=18)
        controls_label = tk.Label(self.master, text="Controls", font=("Helvetica", 12, "bold"), 
                          bg="#f0f0f0", padx=5, pady=5, relief="solid", width=18)

        ### control buttons ###
        font = ("Helvetica", 11)
        bg = "#ebe6e6"
        padx = 5
        pady = 5
        borderwidth = 1
        relief = "solid"
        width = 20

        import_T1_T2_button = tk.Button(self.master, text="Import T1 & T2 MR Images", command=self.loadT1T2Images,
                                        font=font, bg=bg, padx=padx, pady=pady,
                                        borderwidth=borderwidth, relief=relief, width=width)
        register_button = tk.Button(self.master, text="Register", command=self.registerImages,
                                    font=font, bg=bg, padx=padx, pady=pady,
                                    borderwidth=borderwidth, relief=relief, width=width)
        export_button = tk.Button(self.master, text="Export Table to CSV", command=self.exportTables, 
                                  font=font, bg=bg, padx=padx, pady=pady,
                                  borderwidth=borderwidth, relief=relief, width=width)
        reset_button = tk.Button(self.master, text="Reset", command=self.reset,
                                 font=font, bg=bg, padx=padx, pady=pady,
                                 borderwidth=borderwidth, relief=relief, width=width)
        show_disc_masks = tk.Button(self.master, text="Show Disc Masks", command=self.showDiscMasks,
                                    font=font, bg=bg, padx=padx, pady=pady,
                                    borderwidth=borderwidth, relief=relief, width=width)
        show_vertebra_masks = tk.Button(self.master, text="Show Vertebra Masks", command=self.showVertebraMasks,
                                        font=font, bg=bg, padx=padx, pady=pady,
                                        borderwidth=borderwidth, relief=relief, width=width)
        show_create_disc_masks = tk.Button(self.master, text="Create Disc Masks", command=self.showCreateDiscMasks,
                                           font=font, bg=bg, padx=padx, pady=pady,
                                           borderwidth=borderwidth, relief=relief, width=width)
        show_create_vertebra_masks = tk.Button(self.master, text="Create Vertebra Masks", command=self.showCreateVertebraMasks,
                                               font=font, bg=bg, padx=padx, pady=pady,
                                               borderwidth=borderwidth, relief=relief, width=width)

        ### data tables for metrics ###
        # disc table
        self.disc_table = ttk.Treeview(self.master, show='headings', height=5)
        self.disc_table['columns'] = ('Disc', 'DVI', 'Degeneration Score')
        self.disc_table.heading('Disc', text='Disc', anchor='center')
        self.disc_table.heading('DVI', text='DVI', anchor='center')
        self.disc_table.heading('Degeneration Score', text='Degeneration Score', anchor='center')
        self.disc_table.column("Disc", width=100, anchor='center', stretch='no')
        self.disc_table.column("DVI", width=100)
        self.disc_table.column("Degeneration Score", width=300)
        self.initilizeDiscTable()

        # vertebrate table
        self.vertebra_table = ttk.Treeview(self.master, show='headings', height=5)
        self.vertebra_table['columns'] = ('Vertebra', 'Modic')
        self.vertebra_table.heading('Vertebra', text='Vertebra', anchor='center')
        self.vertebra_table.heading('Modic', text='Modic', anchor='center')
        self.vertebra_table.column("Vertebra", width=100, anchor='center', stretch='no')
        self.vertebra_table.column("Modic", width=100)
        self.initilizeVertebraTable()

        ### styling tables ###
        style = ttk.Style()
        style.configure("Treeview", borderwidth=0, highlightthickness=0, 
                        rowheight=20, font=("TkDefaultFont", 12))
        style.configure("Treeview.Heading", font=("TkDefaultFont", 12, "bold"), background="#ececec")

        ### create slider to adjust image grey levels ###
        self.slider = tk.Scale(root, from_=0, to=100, orient=tk.HORIZONTAL, length=520,
                               state='disabled', showvalue=False, command=self.updateGreyLevels)  

        ### GUI layout ###
        self.t1_img_label.grid(row=1, column=0, rowspan=10)
        self.t2_img_label.grid(row=1, column=1, rowspan=10)

        t1_label.grid(row=0, column=0)
        t2_label.grid(row=0, column=1)

        controls_label.grid(row=1, column=2, sticky="ew")

        import_T1_T2_button.grid(row=2, column=2)
        register_button.grid(row=3, column=2)
        export_button.grid(row=4, column=2)
        reset_button.grid(row=5, column=2)
        show_disc_masks.grid(row=6, column=2)
        show_vertebra_masks.grid(row=7, column=2)
        show_create_disc_masks.grid(row=8, column=2)
        show_create_vertebra_masks.grid(row=9, column=2)

        self.slider.grid(row=11, column=0, columnspan=2)
        
        self.disc_table.grid(row=12, column=1, columnspan=1, rowspan=5)
        self.vertebra_table.grid(row=12, column=0, columnspan=1, rowspan=5)

        min_size_button_rows = 57
        self.master.rowconfigure(1, minsize=min_size_button_rows)
        self.master.rowconfigure(2, minsize=min_size_button_rows)
        self.master.rowconfigure(3, minsize=min_size_button_rows)
        self.master.rowconfigure(4, minsize=min_size_button_rows)
        self.master.rowconfigure(5, minsize=min_size_button_rows)
        self.master.rowconfigure(6, minsize=min_size_button_rows)
        self.master.rowconfigure(7, minsize=min_size_button_rows)
        self.master.rowconfigure(8, minsize=min_size_button_rows)
        self.master.rowconfigure(9, minsize=min_size_button_rows)
        self.master.rowconfigure(10, minsize=20)
        
        min_size_img_cols = 520
        self.master.columnconfigure(0, minsize=min_size_img_cols)
        self.master.columnconfigure(1, minsize=min_size_img_cols)
    

    ###############################################################################################
    # open a file dialog to get the MR image
    ###############################################################################################
    def openFile(self):
        filename = filedialog.askopenfilename(filetypes=(("Nifti files", "*.nii *.nii.gz")))
        if filename:
            return filename


    ###############################################################################################
    # load the MR image from the filename into a photo
    ###############################################################################################
    def loadImage(self, filename):
        img = nib.load(filename)
        data = img.get_fdata()
        data = np.moveaxis(data, -1, 0)
        data = (data - data.mean()) / data.std()

        n_slice = data.shape[0] // 2
        
        fig = Figure(figsize=(12, 4), constrained_layout=True)
        ax = fig.add_subplot(1,1,1)
        image = ax.imshow(np.rot90(data[n_slice,:,:], -1), cmap='gray', vmin=-1.0, vmax=1.5)    

        return fig, image, data
    

    ###############################################################################################
    # Load the T1 and T2 images and the vois folder into the application 
    ###############################################################################################
    def loadT1T2Images(self):
        self.loadT1Image()
        self.loadT2Image()
        self.loadVoisFolder()


    ###############################################################################################
    # Load the T1 image into the application
    ###############################################################################################
    def loadT1Image(self):
        self.t1_filepath = self.openFile()
        if not self.t1_filepath == None:
            fig, image, data = self.loadImage(self.t1_filepath)
            self.t1_image_scale = image
            self.t1_canvas = FigureCanvasTkAgg(fig, self.master)
            self.t1_canvas.get_tk_widget().config(width=data.shape[1], height=data.shape[2])
            self.t1_canvas.get_tk_widget().grid(row=1, column=0, rowspan=10)


    ###############################################################################################
    # Load the T2 image into the application
    ###############################################################################################
    def loadT2Image(self):
        self.t2_filepath = self.openFile()
        if not self.t2_filepath == None:
            fig, image, data = self.loadImage(self.t2_filepath)
            self.t2_image_scale = image
            self.t2_canvas = FigureCanvasTkAgg(fig, self.master)
            self.t2_canvas.get_tk_widget().config(width=data.shape[1], height=data.shape[2])
            self.t2_canvas.get_tk_widget().grid(row=1, column=1, rowspan=10)
            self.enableSlider()

    
    ###############################################################################################
    # Load the vois folder into the application
    ###############################################################################################
    def loadVoisFolder(self):
        self.vois_filepath = filedialog.askdirectory()


    ###############################################################################################
    # Enable the slider to be used.
    ###############################################################################################
    def enableSlider(self):
        self.slider.config(state='normal')
        self.slider.set(50)
        self.updateGreyLevels(self.slider.get())


    ###############################################################################################
    # Apply the new grey levels to the image
    ###############################################################################################
    def updateGreyLevels(self, value):
        if self.t1_image_scale is not None and self.t2_image_scale is not None:
            v = float(value) / 100.0 * 2.5
            self.t1_image_scale.set_cmap('gray')
            self.t1_image_scale.set_clim(vmin=-1, vmax=v)
            self.t1_canvas.draw()
            self.t2_image_scale.set_cmap('gray')
            self.t2_image_scale.set_clim(vmin=-1, vmax=v)
            self.t2_canvas.draw()


    ###############################################################################################
    # Register the T1 and T2 images and add metrics to the data table
    ###############################################################################################
    def registerImages(self):
        if(self.canRegister()):
            # TODO
            # register the images (skipping for now)
            # feed T2 image through disc and vertebra models
            # add/create/retrieve metrics
            # add metrics to the table

            # feed image through model
            self.disc_y_pred, self.disc_test_image = disc_model_run(self.t2_filepath, self.vois_filepath, self.disc_model)
            self.vertebra_y_pred, self.vertebra_test_tensor = vertebra_model_run(self.t2_filepath, self.vertebra_model)     
            
            # calculate the disc degeneration score
            # TODO

            # calculate the modic changes
            # TODO

            # update the metric tables
            self.updateMetricsTable()       
    

    ###############################################################################################
    # Open the Disc Mask Figures Window
    ###############################################################################################
    def showDiscMasks(self):
        if not self.predictionsMade('d'):
            return
        if self.disc_figures is None:
            self.disc_figures = create_disc_image_masks(self.disc_y_pred, self.disc_test_image)
        # create a new window
        canvas_window = tk.Toplevel(self.master)
        canvas_window.title("Disc Image Masks")
        canvas_window.geometry("1200x750")
        disc_viewer = prediction_masks_viewer.FigureViewer(master=canvas_window, figures=self.disc_figures)


    ###############################################################################################
    # Open the Vertebra Mask Figures Window
    ###############################################################################################
    def showVertebraMasks(self):
        if not self.predictionsMade('v'):
            return
        if self.vertebra_figures is None:
            self.vertebra_figures = create_vertebra_image_masks(self.vertebra_y_pred, self.vertebra_test_tensor)
        # create a new window
        canvas_window = tk.Toplevel(self.master)
        canvas_window.title("Vertebra Image Masks")
        canvas_window.geometry("1200x750")
        vertebra_viewer = prediction_masks_viewer.FigureViewer(master=canvas_window, figures=self.vertebra_figures)


    ###############################################################################################
    # Open the Create Disc Mask Window
    ###############################################################################################
    def showCreateDiscMasks(self):
        canvas_window = tk.Toplevel(self.master)
        canvas_window.title("Create Disc Masks")
        canvas_window.geometry("1200x750")
        self.create_disc_viewer = create_masks.CreateMasks(master=canvas_window,
                                                           filename=self.t2_filepath,
                                                           button_names=self.disc_names)


    ###############################################################################################
    # Open the Create Vertebra Mask Window
    ###############################################################################################
    def showCreateVertebraMasks(self):
        canvas_window = tk.Toplevel(self.master)
        canvas_window.title("Create Vertebra Masks")
        canvas_window.geometry("1200x750")
        self.create_vertebra_viewer = create_masks.CreateMasks(master=canvas_window,
                                                               filename=self.t2_filepath,
                                                               button_names=self.vertebra_names)


    ###############################################################################################
    # Check to ensure that there are t1 and t2 files
    ###############################################################################################
    def canRegister(self):
        ret = True
        if self.t2_filepath is None:
            messagebox.showinfo("Unable to Register", "Missing T2 MRI.", icon="warning", type="ok", parent=self.master)
            ret = False
        return ret
    

    ###############################################################################################
    # Check to ensure that files ran through models
    ###############################################################################################
    def predictionsMade(self, type):
        ret = True
        if type == 'd' and self.disc_y_pred is None:
            messagebox.showinfo("Unable to Make Masks", "Missing Disc Predictions.", icon="warning", type="ok", parent=self.master)
            ret = False
        elif type == 'v' and self.vertebra_y_pred is None:
            messagebox.showinfo("Unable to Make Masks", "Missing Vertebra Predictions.", icon="warning", type="ok", parent=self.master)
            ret = False
        return ret


    ###############################################################################################
    # Initilize a blank data table with only the disc names
    ###############################################################################################
    def initilizeDiscTable(self):
        self.disc_table.insert("", tk.END, values=("L1L2", "", ""))
        self.disc_table.insert("", tk.END, values=("L2L3", "", ""))
        self.disc_table.insert("", tk.END, values=("L3L4", "", ""))
        self.disc_table.insert("", tk.END, values=("L4L5", "", ""))
        self.disc_table.insert("", tk.END, values=("L5S1", "", ""))

    
    ###############################################################################################
    # Initilize a blank data table with only the disc names
    ###############################################################################################
    def initilizeVertebraTable(self):
        self.vertebra_table.insert("", tk.END, values=("L1", ""))
        self.vertebra_table.insert("", tk.END, values=("L2", ""))
        self.vertebra_table.insert("", tk.END, values=("L3", ""))
        self.vertebra_table.insert("", tk.END, values=("L4", ""))
        self.vertebra_table.insert("", tk.END, values=("L5", ""))


    ###############################################################################################
    # Export the tables to a csv file
    ###############################################################################################
    def exportTables(self):
        with open("data_table.csv", mode="w", newline="") as file:
            writer = csv.writer(file)

            # add disc table
            disc_header_row = [column for column in self.disc_table["columns"]]
            writer.writerow(disc_header_row)
            for item in self.disc_table.get_children():
                row = [self.disc_table.item(item, "values")[column] for column in range(len(self.disc_table["columns"]))]
                writer.writerow(row)

            # blank row to split the data tables
            writer.writerow([])

            # add vertebra table
            vertebra_header_row = [column for column in self.vertebra_table["columns"]]
            writer.writerow(vertebra_header_row)
            for item in self.vertebra_table.get_children():
                row = [self.vertebra_table.item(item, "values")[column] for column in range(len(self.vertebra_table["columns"]))]
                writer.writerow(row)
        
        # success message if tables exported to csv properly
        messagebox.showinfo("Export Success", "Tables Succesfully Exported!", icon="info", type="ok", parent=self.master)


    ###############################################################################################
    # Update the metrics tables
    ###############################################################################################
    def updateMetricsTable(self):
        pass


    ###############################################################################################
    # Reset the appliction
    ###############################################################################################
    def reset(self):
        # clear the tables
        for item in self.disc_table.get_children():
            self.disc_table.delete(item)
        self.initilizeDiscTable()
        for item in self.vertebra_table.get_children():
            self.vertebra_table.delete(item)
        self.initilizeVertebraTable()

        # clear the image labels
        self.t1_img_label.config(image=None)
        self.t2_img_label.config(image=None)

        # clear the photos and image data
        if self.t1_photo is not None:
            self.t1_photo = None
            self.t1_image_data = None
        if self.t2_photo is not None:
            self.t2_photo = None
            self.t2_image_data = None

        # clear prediction figures
        if self.disc_figures is not None:
            self.disc_figures = None
        if self.vertebra_figures is not None:
            self.vertebra_figures = None

        # clear the t1/t2 and vois paths
        if self.t1_filepath is not None:
            self.t1_filepath = None
        if self.t2_filepath is not None:
            self.t2_filepath = None
        if self.vois_filepath is not None:
            self.vois_filepath = None

        # clear the predictions/tensors
        if self.disc_y_pred is not None:
            self.disc_y_pred = None
        if self.disc_test_image is not None:
            self.disc_test_image = None
        if self.vertebra_y_pred is not None:
            self.vertebra_y_pred = None
        if self.vertebra_test_tensor is not None:
            self.vertebra_test_tensor = None

        # clear disc and vertebra create masks viewers
        if self.create_disc_viewer is not None:
            self.create_disc_viewer = None
        if self.create_vertebra_viewer is not None:
            self.create_vertebra_viewer = None
        

# Create the tkinter application and run the mainloop.
if __name__ == '__main__':
    root = tk.Tk()
    ex = QuantitativeMRIMetrics(root)
    root.mainloop()
