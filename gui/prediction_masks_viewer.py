import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk

# This will allow the user to view the prediction masks produced by the disc and vertebra model.
# The user can scroll through the various slices that are contained.
class FigureViewer:
    ###############################################################################################
    # Initialize Variables and Setup.
    ###############################################################################################
    def __init__(self, master, figures):
        self.master = master
        self.master.geometry("800x600")
        self.current_fig_index = 0
        self.figures = figures

        # Create a canvas to display the first figure
        self.canvas = FigureCanvasTkAgg(self.figures[self.current_fig_index], master=self.master)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # Add a navigation toolbar for the figures
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.master)
        self.toolbar.update()
        #self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        self.toolbar.pack(side=tk.BOTTOM, fill=tk.X)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # Add a button to go to the next slice
        self.next_button = tk.Button(master=self.master, text="Next Slice", command=self.show_next_figure)
        self.next_button.pack(side=tk.RIGHT)

        # Add a button to go back to the previous slice
        self.back_button = tk.Button(master=self.master, text="Previous Slice", command=self.show_previous_figure)
        self.back_button.pack(side=tk.RIGHT)


    ###############################################################################################
    # Show the next slice.
    ###############################################################################################
    def show_next_figure(self):
        self.toolbar.zoom() # Reset the zoom
        if self.current_fig_index < len(self.figures) - 1:
            self.current_fig_index += 1
            self.canvas.figure = self.figures[self.current_fig_index]
            self.canvas._tkcanvas.pack_forget() # Remove the old canvas from the window
            self.next_button.pack_forget()
            self.back_button.pack_forget()
            self.canvas = FigureCanvasTkAgg(self.figures[self.current_fig_index], master=self.master) # Create a new canvas with the new figure
            self.canvas.draw()
            self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
            if self.toolbar is not None:
                self.toolbar.pack_forget() # Remove the old toolbar from the window
            self.toolbar = NavigationToolbar2Tk(self.canvas, self.master)
            self.toolbar.update()
            self.toolbar.pack(side=tk.BOTTOM, fill=tk.X)
            self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
            self.next_button.pack(side=tk.RIGHT)
            self.back_button.pack(side=tk.RIGHT)


    ###############################################################################################
    # Show the previous slice.
    ###############################################################################################
    def show_previous_figure(self):
        self.toolbar.zoom() # Reset the zoom
        if self.current_fig_index > 0:
            self.current_fig_index -= 1
            self.canvas.figure = self.figures[self.current_fig_index]
            self.canvas._tkcanvas.pack_forget() # Remove the old canvas from the window
            self.next_button.pack_forget()
            self.back_button.pack_forget()
            self.canvas = FigureCanvasTkAgg(self.figures[self.current_fig_index], master=self.master) # Create a new canvas with the new figure
            self.canvas.draw()
            self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
            if self.toolbar is not None:
                self.toolbar.pack_forget() # Remove the old toolbar from the window
            self.toolbar = NavigationToolbar2Tk(self.canvas, self.master)
            self.toolbar.update()
            self.toolbar.pack(side=tk.BOTTOM, fill=tk.X)
            self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
            self.next_button.pack(side=tk.RIGHT)
            self.back_button.pack(side=tk.RIGHT)
