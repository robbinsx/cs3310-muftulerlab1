# Automatic MRI Analysis to Study Spinal Abnormalities​​
This repository contains experiments looking at discs and vertebrates in the spine of the lower back. For expereiments regarding the discs, the Pfirrman Degeneration Scoring was used when exlporing features in the disc regions that could be used to replicate the grades produced by the Pfirrman Degeneration Scoring. For experiments regarding the discs, the Modic Changes of the vertebral bodies was used when exploring features in the vertebra regions that could be used to replicate the types produced by the Modic Changes Metric.

Additionally, there is a starting GUI application which is used for MRI analysis of an imported T1 and T2 weighted MR image. The application was made in Python and uses the TKinter functionality.

## Directories
- data_pipeline: A pipeline which has various features to set up all of the subject data to be ready for analysis
- gui: A python graphic user interface application used to analyze spinal abormalities
- models: Model creation section for the modic changes and pfirrman degeneration experiments
- modic_changes: Contains experiments and research into modic changes in vertebraes
- pfirrman_degeneration: Contains experiments and research into Pfirrman degeneration in discs
- registration: Exploration on registering T2 weighted MR images on T1 weighted MR images.

## Wiki Pages
Please reference the Wiki to find more information about the GUI Application, Data Pipeline, and other resources/tools used in this repository.