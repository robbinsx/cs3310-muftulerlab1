import numpy as np
import math
import pandas as pd
from scipy import stats
from sklearn.cluster import AgglomerativeClustering


def group_clusters_return_matrix(
    cluster_df, apply_cluster_transformation=None, cluster_labels=None, y_weight=1
):
    result = {}

    grouped_df = cluster_df.groupby("cluster")

    for cluster, cluster_df_body in grouped_df:
        centroid = cluster_df_body[["x", "y"]].mean()
        cluster_matrix = np.zeros((512, 512))
        cluster_matrix[cluster_df_body["y"], cluster_df_body["x"]] = 1
        if apply_cluster_transformation is not None:
            cluster_matrix = apply_cluster_transformation(cluster_matrix)

        result[cluster] = {
            "centroid": [centroid["x"], centroid["y"] * y_weight],
            "matrix": cluster_matrix,
        }

    if cluster_labels is not None and len(result) == len(cluster_labels):
        clustered_res = {}
        sorted_result = sorted(
            result.items(), key=lambda x: x[1]["centroid"], reverse=True
        )
        for [cluster, (_, body)] in zip(cluster_labels, sorted_result):
            clustered_res[cluster] = body
        return clustered_res

    return result


def cluster_segments(
    sample,
    custom_distance_threshold_fn=None,
    apply_cluster_transformation=None,
    y_weight=4,
    cluster_labels=None,
):
    x, y = sample.nonzero()
    data = np.array([x, y * y_weight]).T
    if len(data) == 0:
        return {}

    if custom_distance_threshold_fn is not None:
        distance_threshold = custom_distance_threshold_fn(data)
    else:
        distance_threshold = 1000

    clusters = AgglomerativeClustering(n_clusters=None, 
                                       distance_threshold=distance_threshold, 
                                       linkage='single').fit(data) 
                                       # linkage{‘ward’, ‘complete’, ‘average’, ‘single’}, default=’ward’
    labels = clusters.labels_

    cluster_df = pd.DataFrame(np.array([y, x, labels]).T, columns=["x", "y", "cluster"])
    cluster_df = cluster_df[
        (np.abs(stats.zscore(cluster_df[["x", "y"]])) < 4).all(axis=1)
    ]

    return group_clusters_return_matrix(
        cluster_df,
        apply_cluster_transformation,
        cluster_labels=cluster_labels,
        y_weight=y_weight,
    )


def l2_dist(x, y):
    return math.sqrt((x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)


def label_clusters_from_centroids(clusters, centroids, n_labels):
    if len(clusters) == n_labels:
        return clusters
    res = []
    for cluster, body in clusters:
        smallest_centroid = math.inf
        new_cluster = cluster
        curr_centroid = body["centroid"]
        for centroid_cluster, centroid in centroids.items():
            dist = l2_dist(curr_centroid, centroid)
            if dist < smallest_centroid:
                new_cluster = centroid_cluster
                smallest_centroid = dist
        res.append([new_cluster, body])
    return res


def cluster_segments_scan(
    vois,
    apply_cluster_transformation=None,
    custom_distance_threshold_fn=None,
    y_weight=4,
    cluster_labels=["L5S1", "L4L5", "L3L4", "L2L3", "L1L2"],
):
    segmented_scans = []

    centroids_db = {label: [0, 0] for label in cluster_labels}
    n_of_clusters = 0

    for scan_voi in vois.squeeze():
        clustered_scan = cluster_segments(
            scan_voi,
            apply_cluster_transformation=apply_cluster_transformation,
            custom_distance_threshold_fn=custom_distance_threshold_fn,
            y_weight=y_weight,
            cluster_labels=cluster_labels,
        )
        clustered_scan_items = list(clustered_scan.items())
        segmented_scans.append(clustered_scan_items)
        if len(clustered_scan_items) == len(cluster_labels):
            n_of_clusters += 1
            for cluster, scan_items in clustered_scan_items:
                centroids_db[cluster][0] += scan_items["centroid"][0]
                centroids_db[cluster][1] += scan_items["centroid"][1]

    if n_of_clusters != 0:
        for disc, _ in centroids_db.items():
            centroids_db[disc][0] /= n_of_clusters
            centroids_db[disc][1] /= n_of_clusters

    segmented_scans = [
        label_clusters_from_centroids(
            clustered_scan, centroids_db, n_labels=len(cluster_labels)
        )
        for clustered_scan in segmented_scans
    ]
    return list(
        map(
            lambda x: [
                [cluster, cluster_body["matrix"]] for cluster, cluster_body in x
            ],
            segmented_scans,
        )
    )
