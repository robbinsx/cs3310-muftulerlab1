# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
from tensorflow import keras, config, debugging
import tensorflow.keras.backend as K #LTM
import nibabel as nib
from os import listdir
import math
import pandas as pd
from scipy import stats
from scipy import ndimage
from sklearn.cluster import AgglomerativeClustering
import os #LTM
from dipy.io.utils import create_nifti_header, get_reference_info, is_header_compatible
from dipy.io.image import save_nifti
import sys 
import SimpleITK as sitk
import shutil
import glob
import radiomics 
from radiomics import featureextractor
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import GroupShuffleSplit
import matplotlib.patches as mpatches
import seaborn as sns
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor

os.environ["CUDA_VISIBLE_DEVICES"] = "-1" #LTM
gpu = config.list_physical_devices('GPU') #LTM


## functions
def read_nifti_file(filepath):
    scan = nib.load(filepath)
    scan = scan.get_fdata()
    return np.moveaxis(scan, -1, 0)


def standardize_scans(X):
    means = generate_transform(X, lambda x: x.mean(axis=(-3, -2, -1)))
    stds = generate_transform(X, lambda x: x.std(axis=(-3, -2, -1)))
    return (X - means) / stds


def standardize_scan(X):
    return (X - X.mean()) / X.std()


def get_vois(filedir, identify_discs=False):
    vois_files = list(filter(lambda x: "discT2W.nii" in x, listdir(filedir)))
    
    if identify_discs:
        vois_files.sort()
    
    #print('vois_files', vois_files)
    vois_images = list(map(lambda x: read_nifti_file(f"{filedir}/{x}"), vois_files))
   
    if identify_discs:
        return np.add.reduce(
            [clustered_disc * (i + 1) for i, clustered_disc in enumerate(vois_images)]
        ).astype(int)
    return np.any(vois_images, axis=0).astype(int)


def get_vb_vois(filedir):
    vois_files = list(filter(lambda x: "_vb.nii" in x, listdir(filedir)))
    
    vois_files.sort()
    print(filedir)
    print(vois_files)
    
    L1 = read_nifti_file(f'{filedir}' + vois_files[0])
    L2 = read_nifti_file(f'{filedir}' + vois_files[1])
    L3 = read_nifti_file(f'{filedir}' + vois_files[2])
    L4 = read_nifti_file(f'{filedir}' + vois_files[3])
    L5 = read_nifti_file(f'{filedir}' + vois_files[4])
    
    target = L1 + 2*L2 + 3*L3 + 4*L4 + 5*L5
    print(target)
           
    return target.astype(int) 


def cluster_segments(
    sample,
    custom_distance_threshold_fn=None,
    apply_cluster_transformation=None,
    y_weight=4,
    cluster_labels=None,
):
    x, y = sample.nonzero()
    data = np.array([x, y * y_weight]).T
    if len(data) == 0:
        return {}

    if custom_distance_threshold_fn is not None:
        distance_threshold = custom_distance_threshold_fn(data)
    else:
        distance_threshold = 1000

    clusters = AgglomerativeClustering(n_clusters=None, 
                                       distance_threshold=distance_threshold, 
                                       linkage='single').fit(data) 
                                       # linkage{‘ward’, ‘complete’, ‘average’, ‘single’}, default=’ward’
    labels = clusters.labels_

    cluster_df = pd.DataFrame(np.array([y, x, labels]).T, columns=["x", "y", "cluster"])
    cluster_df = cluster_df[
        (np.abs(stats.zscore(cluster_df[["x", "y"]])) < 4).all(axis=1)
    ]

    return group_clusters_return_matrix(
        cluster_df,
        apply_cluster_transformation,
        cluster_labels=cluster_labels,
        y_weight=y_weight,
    )


def cluster_segments_scan(
    vois,
    apply_cluster_transformation=None,
    custom_distance_threshold_fn=None,
    y_weight=4,
    cluster_labels=["L5S1", "L4L5", "L3L4", "L2L3", "L1L2"],
):
    segmented_scans = []

    centroids_db = {label: [0, 0] for label in cluster_labels}
    n_of_clusters = 0

    for scan_voi in vois.squeeze():
        clustered_scan = cluster_segments(
            scan_voi,
            apply_cluster_transformation=apply_cluster_transformation,
            custom_distance_threshold_fn=custom_distance_threshold_fn,
            y_weight=y_weight,
            cluster_labels=cluster_labels,
        )
        clustered_scan_items = list(clustered_scan.items())
        segmented_scans.append(clustered_scan_items)
        if len(clustered_scan_items) == len(cluster_labels):
            n_of_clusters += 1
            for cluster, scan_items in clustered_scan_items:
                centroids_db[cluster][0] += scan_items["centroid"][0]
                centroids_db[cluster][1] += scan_items["centroid"][1]

    if n_of_clusters != 0:
        for disc, _ in centroids_db.items():
            centroids_db[disc][0] /= n_of_clusters
            centroids_db[disc][1] /= n_of_clusters

    segmented_scans = [
        label_clusters_from_centroids(
            clustered_scan, centroids_db, n_labels=len(cluster_labels)
        )
        for clustered_scan in segmented_scans
    ]
    return list(
        map(
            lambda x: [
                [cluster, cluster_body["matrix"]] for cluster, cluster_body in x
            ],
            segmented_scans,
        )
    )


def group_clusters_return_matrix(
    cluster_df, apply_cluster_transformation=None, cluster_labels=None, y_weight=1
):
    result = {}

    grouped_df = cluster_df.groupby("cluster")

    for cluster, cluster_df_body in grouped_df:
        centroid = cluster_df_body[["x", "y"]].mean()
        cluster_matrix = np.zeros((512, 512))
        cluster_matrix[cluster_df_body["y"], cluster_df_body["x"]] = 1
        if apply_cluster_transformation is not None:
            cluster_matrix = apply_cluster_transformation(cluster_matrix)

        result[cluster] = {
            "centroid": [centroid["x"], centroid["y"] * y_weight],
            "matrix": cluster_matrix,
        }

    if cluster_labels is not None and len(result) == len(cluster_labels):
        clustered_res = {}
        sorted_result = sorted(
            result.items(), key=lambda x: x[1]["centroid"], reverse=True
        )
        for [cluster, (_, body)] in zip(cluster_labels, sorted_result):
            clustered_res[cluster] = body
        return clustered_res

    return result


def label_clusters_from_centroids(clusters, centroids, n_labels):
    if len(clusters) == n_labels:
        return clusters
    res = []
    for cluster, body in clusters:
        smallest_centroid = math.inf
        new_cluster = cluster
        curr_centroid = body["centroid"]
        for centroid_cluster, centroid in centroids.items():
            dist = l2_dist(curr_centroid, centroid)
            if dist < smallest_centroid:
                new_cluster = centroid_cluster
                smallest_centroid = dist
        res.append([new_cluster, body])
    return res


def transfer_img(img):
    img = ndimage.rotate(img, 90, reshape=False)
    return np.flip(img)


def l2_dist(x, y):
    return math.sqrt((x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)


## clustering
cluster_colors = {
    "T12": "cyan",
    "L1": "orange",
    "L2": "yellow",
    "L3": "blue",
    "L4": "green",
    "L5": "red",
    "undefined": "pink",
}

cluster_colors_disc = {
    "L1L2": "cyan",
    "L2L3": "orange",
    "L3L4": "red",
    "L4L5": "blue",
    "L5S1": "green",
    "undefined": "pink",
}

matplotlib_cluster_colors = {
    k: matplotlib.colors.ListedColormap(["none", v]) for k, v in cluster_colors.items()
}

matplotlib_cluster_colors_disc = {
    k: matplotlib.colors.ListedColormap(["none", v]) for k, v in cluster_colors_disc.items()
}
    
    
def disc_signal_intensity(ts1, ts1_datapath, ts1_vois_path, ts2, ts2_datapath, ts2_vois_path, spine_data_path, save_hist_path, save_hist=True):
    """
    This function will calculate the signal intensity in the disc prediction masks of two patients. A bar plot will be
    made to compare the two patients. This can be used to compare healthy vs healthy, healthy vs non-healthy, and
    non-healthy vs non-healthy.
    :param ts1: Subject 1 (string)
    :param ts1_datapath: Path to subject 1 directory, excluding the 'S###'
    :param ts1_vois_path: Path to subject 1 vois directory
    :param ts2: Subject 2 (string)
    :param ts2_datapath: Path to subject 2 directory, excluding the 'S###'
    :param ts2_vois_path: Path to subject 2 vois directory
    :param spine_data_path: File path to spine csv file
    :param save_hist_path: File directory to save the histograms
    :param save_hist: True (default) if save the histograms, False otherwise
    :return signal_intensities_ts1_dict: A dictionary of subject 1 signal intensities
    :return signal_intensities_ts2_dict: A dictionary of subject 2 signal intensities
    """
    image_ts1 = sitk.ReadImage(f'{ts1_datapath}{ts1}/{ts1.lower()}_T2.nii.gz')
    mask_L5S1 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L5S1.nii')
    mask_L1L2 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L1L2.nii')
    mask_L2L3 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L2L3.nii')
    mask_L3L4 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L3L4.nii')
    mask_L4L5 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L4L5.nii')
    mask_ls_ts1 = [mask_L5S1,mask_L1L2,mask_L2L3,mask_L3L4,mask_L4L5]

    image_ts2 = sitk.ReadImage(f'{ts2_datapath}{ts2}/{ts2.lower()}_T2.nii.gz')
    mask_L5S1 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L5S1.nii')
    mask_L1L2 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L1L2.nii')
    mask_L2L3 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L2L3.nii')
    mask_L3L4 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L3L4.nii')
    mask_L4L5 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L4L5.nii')
    mask_ls_ts2 = [mask_L5S1,mask_L1L2,mask_L2L3,mask_L3L4,mask_L4L5]

    spine_data_labels = pd.read_csv(spine_data_path)

    patient_ts1 = spine_data_labels[spine_data_labels[' Subject number '] == int(ts1[1:])]
    patient_ts1 = list(patient_ts1['Pfirrman_degeneration_score'])

    patient_ts2 = spine_data_labels[spine_data_labels[' Subject number '] == int(ts2[1:])]
    patient_ts2 = list(patient_ts2['Pfirrman_degeneration_score']) # changed to list to iterate easily during plotting

    labels = ['L5S1','L1L2','L2L3','L3L4','L4L5']
    ugh = ['o','p']
    
    signal_intensities_ts1_dict = {}
    signal_intensities_ts2_dict = {}
    for mask_ts1, mask_ts2, label, pts1, pts2 in zip(mask_ls_ts1, mask_ls_ts2, labels, patient_ts1, patient_ts2):
        #----------------------------------------------------------------
        # Patient ts1 - degneration
        # Get the numpy arrays of the image and mask
        image_array = sitk.GetArrayFromImage(image_ts1)
        mask_array = sitk.GetArrayFromImage(mask_ts1)
        # Apply the mask to the image
        masked_image_array = np.ma.masked_array(image_array[7:10,:,:], np.logical_not(mask_array[7:10,:,:]))
        # Get the signal intensity values within the ROI
        signal_intensities_ts1 = masked_image_array.compressed()
        signal_intensities_ts1_dict[label] = signal_intensities_ts1

        #-----------------------------------------------------------------
        # Patient ts2 - healthy
        image_array = sitk.GetArrayFromImage(image_ts2)
        mask_array = sitk.GetArrayFromImage(mask_ts2)
        # Apply the mask to the image
        masked_image_array = np.ma.masked_array(image_array[7:10,:,:], np.logical_not(mask_array[7:10,:,:]))
        # Get the signal intensity values within the ROI
        signal_intensities_ts2 = masked_image_array.compressed()
        signal_intensities_ts2_dict[label] = signal_intensities_ts2

        #-----------------------------------------------------------------
        # Plot a histogram of the signal intensity values
        fig, ax = plt.subplots()
        ax.hist(signal_intensities_ts1, bins=50, alpha=0.33, label=f'\nPatient {ts1}\nScore: {pts1}')
        ax.hist(signal_intensities_ts2, bins = 50, alpha=0.33, label =f'\nPatient {ts2}\nScore: {pts2}')
        ax.set_xlabel('Signal intensity')
        ax.set_title(f'{label}')
        ax.legend()
        ax.set_ylabel('Frequency')   
        if save_hist:
            plt.savefig(f'{save_hist_path}{ts1}_{ts2}_{label}_disc_signal_intensity.png')
        plt.show()
        plt.close()
        
    return signal_intensities_ts1_dict, signal_intensities_ts2_dict
        

def vertebra_signal_intensity(ts1, ts1_datapath, ts1_vois_path, ts2, ts2_datapath, ts2_vois_path, spine_data_path, save_hist_path, save_hist=True):
    """
    This function will calculate the signal intensity in the vertebra prediction masks of two patients. A bar plot will be
    made to compare the two patients. This can be used to compare healthy vs healthy, healthy vs non-healthy, and
    non-healthy vs non-healthy.
    :param ts1: Subject 1 (string)
    :param ts1_datapath: Path to subject 1 directory, excluding the 'S###'
    :param ts1_vois_path: Path to subject 1 vois directory
    :param ts2: Subject 2 (string)
    :param ts2_datapath: Path to subject 2 directory, excluding the 'S###'
    :param ts2_vois_path: Path to subject 2 vois directory
    :param spine_data_path: File path to spine csv file
    :param save_hist_path: File directory to save the histograms
    :param save_hist: True (default) if save the histograms, False otherwise
    """
    image_ts1 = sitk.ReadImage(f'{ts1_datapath}{ts1}/{ts1.lower()}_T2.nii.gz')
    T12_ts1 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_T12_vb.nii')
    L1_ts1 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L1_vb.nii')
    L2_ts1 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L2_vb.nii')
    L3_ts1 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L3_vb.nii')
    L4_ts1 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L4_vb.nii')
    L5_ts1 = sitk.ReadImage(f'{ts1_vois_path}{ts1}_L5_vb.nii')
    vb_masks_ts1 = {'T12':T12_ts1, 'L1':L1_ts1, 'L2':L2_ts1, 'L3':L3_ts1, 'L4':L4_ts1, 'L5':L5_ts1}

    image_ts2 = sitk.ReadImage(f'{ts2_datapath}{ts2}/{ts2.lower()}_T2.nii.gz')
    T12_ts2 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_T12_vb.nii')
    L1_ts2 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L1_vb.nii')
    L2_ts2 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L2_vb.nii')
    L3_ts2 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L3_vb.nii')
    L4_ts2 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L4_vb.nii')
    L5_ts2 = sitk.ReadImage(f'{ts2_vois_path}{ts2}_L5_vb.nii')
    vb_masks_ts2 = {'T12':T12_ts2, 'L1':L1_ts2, 'L2':L2_ts2, 'L3':L3_ts2, 'L4':L4_ts2, 'L5':L5_ts2}

    spine_data_labels = pd.read_csv(spine_data_path)

    for mask_ts1, mask_ts2 in zip(vb_masks_ts1.items(), vb_masks_ts2.items()): 
        # Get the numpy arrays of the image and mask 
        image_array = sitk.GetArrayFromImage(image_ts1) 
        mask_array = sitk.GetArrayFromImage(mask_ts1[1]) 
        # Apply the mask to the image 
        masked_image_array = np.ma.masked_array(image_array[7:10,:,:], np.logical_not(mask_array[7:10,:,:])) 
        # Get the signal intensity values within the ROI 
        signal_intensities_ts1 = masked_image_array.compressed() 
        image_array = sitk.GetArrayFromImage(image_ts2) 
        mask_array = sitk.GetArrayFromImage(mask_ts2[1]) 
        # Apply the mask to the image 
        masked_image_array = np.ma.masked_array(image_array[7:10,:,:], np.logical_not(mask_array[7:10,:,:])) 
        # Get the signal intensity values within the ROI 
        signal_intensities_ts2 = masked_image_array.compressed() 
        # Plot a histogram of the signal intensity values 
        fig, ax = plt.subplots() 
        ax.hist(signal_intensities_ts1, bins=50, label=ts1, alpha=0.7) 
        ax.hist(signal_intensities_ts2, bins=50, label=ts2, alpha=0.7) 
        ax.set(xlim=(0, 1300), ylim=(0, 600))
        ax.set_title(f'{ts1} vs {ts2} {mask_ts1[0]} Vertebrae Signal Distribution')
        ax.set_xlabel('Signal intensity') 
        ax.set_ylabel('Frequency') 
        ax.legend()
        if save_hist:
            plt.savefig(f'{save_hist_path}{ts1}_{ts2}_{mask_ts1[0]}_disc_signal_intensity.png')
        plt.show()
        plt.close()



# -

def apply_disc_model(subject, subject_t2_path, vois_path, create_directories_path, disc_model, show_plots, create_plots=True):
    """
    Apply the disc mask prediction model to a subject.
    :param subject: Subject to examine (string)
    :param subject_t2_path: Path to T2 weighted MR image for subject
    :param vois_path: Path to vois directory for subject
    :param create_directories_path: Path to create directory and save prediction mask files
    :param disc_model: The disc prediction model
    :param show_plots: Output the plots in the terminal/notebook (bool)
    :param create_plots: True (default) if prediction masks plots will be made, False otherwise
    """
    # get test file
    test_file = subject_t2_path
    test_image = read_nifti_file(test_file)

    # Input images are normalized. So, need to normalize any image for testing
    test_image = standardize_scan(test_image)
    test_tensor = K.constant(test_image.reshape(*test_image.shape, 1))

    # with the multiclass, it expects an initial estimate of full set of disc masks
    mask_image = get_vois(vois_path, identify_discs=True)
    mask_tensor = K.constant(mask_image.reshape(*test_image.shape, 1))

    test_tensor = K.constant(np.stack((test_image, mask_image), axis = 3))

    # predict disc
    disc_y_pred = disc_model.predict(test_tensor)
    disc_y_pred = (disc_y_pred > 0.5).astype(int)

    n_slices = test_image.shape[0]
    
    subject_files_dir_path = create_directories_path + f'/{subject}'

    # plot disc clusters
    if create_plots:
        fig, axs = plt.subplots(2, 8, figsize=(30,8))
        clusters = ['L1L2', 'L2L3', 'L3L4', 'L4L5', 'L5S1']
        plt.suptitle(f'{subject} T2 Weighted Scan Slices with Clustered Disc Segmentation', fontsize=24)

        for i, ax in zip(range(n_slices), axs.flatten()):
            ax.imshow(np.rot90(test_image[i,:,:],-1),cmap="gray", vmin=-1.0, vmax=1.5) 
            ax.set_axis_off()

            for n,cluster in zip(range(1,6), clusters):
                # note that n multi-class segmentation, channel 0 is background and discs are channels 1-5
                ax.imshow(np.rot90(disc_y_pred[i,:,:,n],-1), cmap=matplotlib_cluster_colors_disc[cluster], alpha=0.5, label=cluster)

        patches = [matplotlib.patches.Patch(color=color, label=cluster) for cluster, color in cluster_colors_disc.items()]
        plt.legend(handles=patches)
        plt.savefig(f'{subject_files_dir_path}/{subject}_clustered.png')
        if show_plots:
            plt.show()
        plt.close()

    L1L2 = np.zeros((test_image.shape[1], test_image.shape[2], n_slices))
    L2L3 = np.zeros((test_image.shape[1], test_image.shape[2], n_slices))
    L3L4 = np.zeros((test_image.shape[1], test_image.shape[2], n_slices))
    L4L5 = np.zeros((test_image.shape[1], test_image.shape[2], n_slices))
    L5S1 = np.zeros((test_image.shape[1], test_image.shape[2], n_slices))

    # note that n multi-class segmentation, channel 0 is background and discs are channels 1-5
    L1L2 = disc_y_pred[:,:,:,1]
    L2L3 = disc_y_pred[:,:,:,2]
    L3L4 = disc_y_pred[:,:,:,3]
    L4L5 = disc_y_pred[:,:,:,4]
    L5S1 = disc_y_pred[:,:,:,5]

    # need to swap axes to save binary disc masks in correct orientation
    L1L2 = np.moveaxis(L1L2, 0, -1)
    L2L3 = np.moveaxis(L2L3, 0, -1) 
    L3L4 = np.moveaxis(L3L4, 0, -1) 
    L4L5 = np.moveaxis(L4L5, 0, -1) 
    L5S1 = np.moveaxis(L5S1, 0, -1) 

    # get the nifti header info from original nifti T2 image
    affine, dimensions, voxel_sizes, voxel_order = get_reference_info(test_file)

    # save disc predictions
    nifti_header = create_nifti_header(affine, dimensions, voxel_sizes)
    save_nifti(subject_files_dir_path + f'/{subject}_L1L2' , L1L2, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L2L3' , L2L3, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L3L4' , L3L4, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L4L5' , L4L5, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L5S1' , L5S1, affine, hdr=nifti_header)


def apply_vertebra_model(subject, subject_t2_path, create_directories_path, vertebra_model, show_plots, create_plots=True):
    """
    Apply the vertebra model to a subject.
    Apply the disc mask prediction model to a subject.
    :param subject: Subject to examine (string)
    :param subject_t2_path: Path to T2 weighted MR image for subject
    :param create_directories_path: Path to create directory and save prediction mask files
    :param vertebra_model: The vertebra prediction model
    :param show_plots: Output the plots in the terminal/notebook (bool)
    :param create_plots: True (default) if prediction masks plots will be made, False otherwise
    """
    # get test file
    test_file = subject_t2_path
    test_image = read_nifti_file(test_file)

    # Input images are normalized. So, need to normalize any image for testing
    test_image = standardize_scan(test_image)
    test_tensor = K.constant(test_image.reshape(*test_image.shape, 1))
    
    # predict vertebra
    vertebra_y_pred = vertebra_model.predict(test_tensor)
    vertebra_y_pred = (vertebra_y_pred > 0.5).astype(int)

    clustered_vois = cluster_segments_scan(vertebra_y_pred, 
                                                      custom_distance_threshold_fn=lambda _: 10,
                                                      cluster_labels=["L5", "L4", "L3", "L2", "L1", "T12"], 
                                                      y_weight=2)
    
    subject_files_dir_path = create_directories_path + f'/{subject}'
    
    ## plot vertebra clusters
    if create_plots:
        plt.figure(figsize=(24, 8), constrained_layout=True)
        plt.suptitle(f'{subject} T2 Weighted Scan Slices with Clustered Vertebra Segmentation', fontsize=24)

        for m in range(16):
            plt.subplot(2, 8, m+1)
            plt.imshow(np.rot90(test_tensor[m], -1), cmap="gray", vmin=-1, vmax=1)
            for [cluster, cluster_vois] in clustered_vois[m]:
                vois_img = transfer_img(cluster_vois)

                if cluster not in matplotlib_cluster_colors:
                    cluster = 'undefined'

                plt.imshow(vois_img, cmap=matplotlib_cluster_colors[cluster], alpha=0.5, label=cluster)

            plt.axis('off')

        patches = [matplotlib.patches.Patch(color=color, label=cluster) for cluster, color in cluster_colors.items()]
        plt.legend(handles=patches)
        plt.savefig(f'{subject_files_dir_path}/{subject}_vertebra_clustered.png')
        if show_plots:
            plt.show()
        plt.close()

    n_slices = test_tensor.shape[0]
    
    T12 = np.zeros((test_tensor.shape[1], test_tensor.shape[2], n_slices))
    L1 = np.zeros((test_tensor.shape[1], test_tensor.shape[2], n_slices))
    L2 = np.zeros((test_tensor.shape[1], test_tensor.shape[2], n_slices))
    L3 = np.zeros((test_tensor.shape[1], test_tensor.shape[2], n_slices))
    L4 = np.zeros((test_tensor.shape[1], test_tensor.shape[2], n_slices))
    L5 = np.zeros((test_tensor.shape[1], test_tensor.shape[2], n_slices))

    for slc in range(n_slices): # loop over slices

        for [cluster, cluster_vois] in clustered_vois[slc]:

            if (cluster == 'T12'):
                T12[:,:,slc] = cluster_vois

            elif (cluster == 'L1'):
                L1[:,:,slc] = cluster_vois

            elif (cluster == 'L2'):
                L2[:,:,slc] = cluster_vois

            elif (cluster == 'L3'):
                L3[:,:,slc] = cluster_vois

            elif (cluster == 'L4'):
                L4[:,:,slc] = cluster_vois

            elif (cluster == 'L5'):
                L5[:,:,slc] = cluster_vois

    affine, dimensions, voxel_sizes, voxel_order = get_reference_info(test_file)

    # save vertebra predictions
    nifti_header = create_nifti_header(affine, dimensions, voxel_sizes)
    save_nifti(subject_files_dir_path + f'/{subject}_T12_vb' , T12, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L1_vb' , L1, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L2_vb' , L2, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L3_vb' , L3, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L4_vb' , L4, affine, hdr=nifti_header)
    save_nifti(subject_files_dir_path + f'/{subject}_L5_vb' , L5, affine, hdr=nifti_header)


def delete_prediction_files(subject, file_directory):
    """
    Delete the prediction files, created by the apply_model_all function in a given file directory.
    :param subject: The subject string (ex: S245)
    :param file_directory: The directory locating all of the prediction files.
    """
    files_to_delete = [
                        file_directory + f'/{subject}/{subject}_L1L2.nii',
                        file_directory + f'/{subject}/{subject}_L2L3.nii',
                        file_directory + f'/{subject}/{subject}_L3L4.nii',
                        file_directory + f'/{subject}/{subject}_L4L5.nii',
                        file_directory + f'/{subject}/{subject}_L5S1.nii',
                        file_directory + f'/{subject}/{subject}_L1_vb.nii',
                        file_directory + f'/{subject}/{subject}_L2_vb.nii',
                        file_directory + f'/{subject}/{subject}_L3_vb.nii',
                        file_directory + f'/{subject}/{subject}_L4_vb.nii',
                        file_directory + f'/{subject}/{subject}_L5_vb.nii',
                        file_directory + f'/{subject}/{subject}_T12_vb.nii'
                      ]
    path = file_directory + f'/{subject}/'
    for file_name in os.listdir(path):
        file = path + file_name
        if os.path.isfile(file) and file in files_to_delete:
            os.remove(file)


def get_subject_list(file_path_spine_data):
    """
    Return a list of all the subjects from the Spine_data.csv which will be used for predictions and analysis.
    :param file_path_spine_data: The csv file for the spine data
    :return: a list of the subject numbers
    """
    df = pd.read_csv(file_path_spine_data)
    return list(set(df[' Subject number '].to_list())) 


def apply_model_all(create_directories_path, create_plots, show_plots, file_path_spine_data):
    '''
    Will go through all of the patients and apply both models to it to get the various prediction files of disc and vertebra
    masks, as well generate plots of the prediction masks overlaying the MR image of the spine.
    The subject_t2_path and subject_vois_path will need to be updated to the appropriate location of the directories/files
    :param create_directories_path: The path location in which to create a new directory
    :param create_plots: True if prediction plots will be made, False otherwise
    :param show_plots: True if plots will be displayed, False otherwise
    :param file_path_spine_data: The file location to the spine data csv file
    '''
    # create models
    disc_h5_file = 'unet-discs_multiclass.h5'
    disc_model = keras.models.load_model(disc_h5_file)
    
    vertebra_h5_file = 'unet-augmented-512new.h5'
    vertebra_model = keras.models.load_model(vertebra_h5_file)
    
    # create the directory to store everything
    if not os.path.exists(create_directories_path):
        os.mkdir(create_directories_path)

    # get all subjects
    all_subjects = get_subject_list(file_path_spine_data)

    # create labels for the disc and vertebra
    disc_labels = ['L5S1','L1L2','L2L3','L3L4','L4L5']
    vertebra_labels = ['L1', 'L2', 'L3', 'L4', 'L5', 'T12']
    ugh = ['o','p']
    
    # store a list for subjects that skipped the model predictions or were successful in the model predictions
    successful = []
    skipped = []
    
    # create prediction masks for each subject
    for subject in all_subjects:
        subject_str = 'S' + str(subject)
        
        # change the T2 path if needed - currently going to given spine data available on ROSIE
        subject_t2_path = f'/data/cs3310/MuftulerLab1/data/spine/{subject_str}/{subject_str.lower()}_T2.nii.gz'

        # other spelling for T2 path 
        subject_T2_path = f'/data/cs3310/MuftulerLab1/data/spine/{subject_str}/{subject_str}_T2.nii.gz'
        
        # change the vois path if needed - currently going to given spine data avaiable on ROSIE
        subject_vois_path = f'/data/cs3310/MuftulerLab1/data/spine/{subject_str}/vois/'
        
        # check if vois folder exists for subject
        if not os.path.exists(subject_vois_path):
            print(f'{subject_str} does not contain a vois folder. Skipping subject')
            skipped.append(subject_str)
            continue
            
        # check if patient contains T2 file path
        if not (os.path.exists(subject_t2_path) or os.path.exists(subject_T2_path)):
            print(f'Path to T1/T2 file does not exist for {subject_str}. Skipping subject')
            skipped.append(subject_str)
            continue   
            
        # check if subject has proper initial estatme for discs
        vois_vertebrae = glob.glob(os.path.join(subject_vois_path, '*discT2W.nii'))
        if len(vois_vertebrae) != 5:
            print(f'{subject_str} has invalid count for initial estimate for discs. Skipping subject')
            skipped.append(subject_str)
            continue
        
        # create directory to store subject prediction files
        subject_files_dir_path = create_directories_path + f'/{subject_str}'
        if not os.path.exists(subject_files_dir_path):
            # make directory
            os.mkdir(subject_files_dir_path)
            # apply both models to subject
            apply_disc_model(subject_str, subject_t2_path, subject_vois_path, create_directories_path, disc_model, show_plots, create_plots)
            apply_vertebra_model(subject_str, subject_t2_path, create_directories_path, vertebra_model, show_plots, create_plots)
            successful.append(subject_str)
        else:
            # if patient already has a directory, the model predictions will be skipped (assuming already created)
            print(f'{subject_str} already has a created directory. Skipping subject')
            skipped.append(subject_str)
            
    print('Done Applying Models to Subjects')
    print(f'Successful Subjects: {successful}')
    print(f'Skipped Subjects: {skipped}')


def get_disc_rad_features(extractor, scan, vois, subject, debug=False):
    """
    Gathers a feature matrix for each segmented disc from the given patient
    :param extractor: PyRadiomics feature extractor to use
    :param scan: Path to Patient MRI Scan
    :param vois: List of Paths to Patient disc VOIs 
    :param debug: If True prints debug info about extractor, defaults to False
    :return rad_feature: a dictionary of radiomic features
    """
    if (debug): 
        print('Extraction parameters:\n\t', extractor.settings)
        print('Enabled filters:\n\t', extractor.enabledImagetypes)
        print('Enabled features:\n\t', extractor.enabledFeatures)

    all_disc_features = []
    for voi in vois: 
        #print(f'Extracting Features from {voi} Mask')
        features = extractor.execute(scan, voi) # Do i do voxel-wise or segmentation based feature extraction?? 
        all_disc_features.append(features)

    # obtaining feature names
    valid_features = list(sorted(filter(lambda x : x.startswith('original_'), all_disc_features[0])))
    valid_features = np.asarray(valid_features)

    rad_feature = np.zeros((valid_features.shape[0], len(all_disc_features)))

    # Loop through all disc feature dicts 
    for i in range(len(all_disc_features)):
        feature_vals = np.array([]) # = []
        # For every valid feature append its value in the current disc feature dict (features)
        # to the ndarray feature_vals
        for feature_name in valid_features:
            feature_vals = np.append(feature_vals, all_disc_features[i][feature_name])

        rad_feature[:, i] = feature_vals

    return rad_feature


def get_vb_rad_features(extractor, scan, vois, debug=False):
    """
    Gathers a feature matrix for each segmented vertebrae from the given patient
    :param extractor: PyRadiomics feature extractor to use
    :param scan: Path to Patient MRI Scan
    :param vois: List of Paths to Patient VB VOIs 
    :param debug: If True prints debug info about extractor, defaults to False
    :return rad_feature: a dictionary of radiomic features
    """
    if (debug): 
        print('Extraction parameters:\n\t', extractor.settings)
        print('Enabled filters:\n\t', extractor.enabledImagetypes)
        print('Enabled features:\n\t', extractor.enabledFeatures)

    all_vb_features = []
    for voi in vois: 
        #print(f'Extracting Features from {voi} Mask')
        features = extractor.execute(scan, voi) # Do i do voxel-wise or segmentation based feature extraction?? 
        all_vb_features.append(features)

    valid_features = list(sorted(filter(lambda x : x.startswith('original_'), all_vb_features[0])))
    valid_features = np.asarray(valid_features)

    rad_feature = np.zeros((valid_features.shape[0], len(all_vb_features)))

    # Loop through all vertebrae feature dicts 
    for x in range(len(all_vb_features)):
        feature_vals = np.array([]) # = []
        # For every valid feature append its value in the current vb feature dict (features)
        # to the ndarray feature_vals
        for feature_name in valid_features:
            feature_vals = np.append(feature_vals, all_vb_features[x][feature_name])

        rad_feature[:, x] = feature_vals

    return rad_feature


def lda_disc_extraction_features(filepath):
    '''
    Peforms LDA on the disc extracted features
    :param filepath: the filepath to disc extracted files csv for disc segments
    :return X_train: predicted training set
    :return y_train: target training set
    :return X_test: predicted testing set
    :return y_test: target testing set
    :return X_valid: predicted validation set
    :return y_valid: target validation set
    '''
    df = pd.read_csv(filepath, index_col = 0)
    df['Pfirrman_degeneration_score'] = df['Pfirrman_degeneration_score'].astype('category')
    
    # performing train/test/valid split
    # extract the patient ID from each index label
    df['patient_id'] = df.index.str.split('_').str[0]

    # define the features and target variable
    X = df.drop(columns=['Pfirrman_degeneration_score','ODI']) 
    y = df['Pfirrman_degeneration_score']

    # define the groups (in your case, patient IDs)
    groups = df['patient_id']

    # define the GroupShuffleSplit with the desired test_size
    gss = GroupShuffleSplit(n_splits=1, test_size=0.20, random_state=66)

    # loop over the splits to get the indices for the training and testing sets
    for train_idx, valid_idx in gss.split(X, y, groups):
        # extract the training and testing data based on the indices
        X_train, y_train = X.iloc[train_idx], y.iloc[train_idx]
        X_valid, y_valid = X.iloc[valid_idx], y.iloc[valid_idx]
        
    # define the features and target variable
    X = X_train.drop(columns=['patient_id']) 
    y = y_train

    # define the groups (in your case, patient IDs)
    groups = X_train['patient_id']

    # define the GroupShuffleSplit with the desired test_size
    gss = GroupShuffleSplit(n_splits=1, test_size=0.20, random_state=4)

    # loop over the splits to get the indices for the training and testing sets
    for train_idx, test_idx in gss.split(X, y, groups):
        # extract the training and testing data based on the indices
        X_train, y_train = X.iloc[train_idx], y.iloc[train_idx]
        X_test, y_test = X.iloc[test_idx], y.iloc[test_idx]
        
    X_valid = X_valid.drop(columns='patient_id')
    
    # performing LDA on training set
    clf = LinearDiscriminantAnalysis()
    clf.fit(X_train, y_train)
    clf.get_feature_names_out(input_features=None)
    
    # transforming all featuer sets with fitted LDA model
    X_train = clf.transform(X_train)
    X_test = clf.transform(X_test)
    X_valid = clf.transform(X_valid)
    
    # saving feature sets as dataframes
    X_train = pd.DataFrame(X_train)
    y_train = pd.DataFrame(y_train)

    X_test = pd.DataFrame(X_test)
    y_test = pd.DataFrame(y_test)

    X_valid = pd.DataFrame(X_valid)
    y_valid = pd.DataFrame(y_valid)
    
    return X_train, y_train, X_test, y_test, X_valid, y_valid


def run_disc_radiomic_classifier_models(X_train, y_train, X_test, y_test, X_val, y_val):
    '''
    Creates and performs various classification models on the data. The accuracy, mean squared
    error (mse), and per class accuracy is stored.
    :param X_train: predicted training set
    :param y_train: target training set
    :param X_test: predicted testing set
    :param y_test: target testing set
    :param X_valid: predicted validation set
    :param y_valid: target validation set
    :return models: A list of tuples containing the model name, accuracy, mse, and per class accuracy
    '''
    names = [
        "Nearest Neighbors",
        "Linear SVM",
        "RBF SVM",
        "Gaussian Process",
        "Decision Tree",
        "Random Forest",
        "Neural Net",
        "AdaBoost",
        "Naive Bayes",
        "QDA",
    ]

    classifiers = [
        KNeighborsClassifier(3),
        SVC(kernel="linear", C=0.025),
        SVC(gamma=2, C=1),
        GaussianProcessClassifier(1.0 * RBF(1.0)),
        DecisionTreeClassifier(max_depth=5),
        RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
        MLPClassifier(alpha=1, max_iter=1000),
        AdaBoostClassifier(),
        GaussianNB(),
        QuadraticDiscriminantAnalysis(),
    ]

    models = []
    for name, classifier in zip(names, classifiers):
        classifier.fit(X_train, y_train)
        prediction = classifier.predict(X_val)
        accuracy = accuracy_score(y_val, prediction)
        mse = mean_squared_error(y_val, prediction)
        matrix = confusion_matrix(y_val, prediction)
        models.append((name, accuracy, mse, matrix))
    
    return models


def run_disc_radiomic_regression_models(X_train, y_train, X_test, y_test, X_val, y_val):
    '''
    Creates and performs various regression models on the data. The mean squared error (mse)
    and rounded mse is recorded.
    :param X_train: predicted training set
    :param y_train: target training set
    :param X_test: predicted testing set
    :param y_test: target testing set
    :param X_valid: predicted validation set
    :param y_valid: target validation set
    :return models: A list of tuples containing the model name, mse, rounded mse
    '''
    regress_names = [
        #"Logistic Regression",
        "Decision Tree",
        "Random Forrest",
        "LinearRegression",
        "Gradient Boost",
    ]

    regressors = [
        #LogisticRegression(),
        DecisionTreeRegressor(),
        RandomForestRegressor(),
        LinearRegression(),
        GradientBoostingRegressor(),
    ]

    models = []
    for name, regressor in zip(regress_names, regressors):
        regressor.fit(X_train, y_train)
        prediction = regressor.predict(X_val)
        mse = mean_squared_error(y_val, prediction)
        rounded_mse = mean_squared_error(np.round(y_val), np.round(prediction))
        models.append((name, mse, rounded_mse))
    
    return models
