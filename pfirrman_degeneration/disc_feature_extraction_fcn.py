# necessary libraries
import radiomics 
from radiomics import featureextractor
import numpy as np

def get_disc_rad_features(extractor, scan, vois, debug=False):
    """
    Gathers a feature matrix for each segmented disc from the given patient
    :param extractor: PyRadiomics feature extractor to use
    :param scan: Path to Patient MRI Scan
    :param vois: List of Paths to Patient disc VOIs 
    :param debug: If True prints debug info about extractor, defaults to False
    """
    if (debug): 
        print('Extraction parameters:\n\t', extractor.settings)
        print('Enabled filters:\n\t', extractor.enabledImagetypes)
        print('Enabled features:\n\t', extractor.enabledFeatures)

    all_disc_features = []
    for voi in vois: 
        print(f'Extracting Features from {voi} Mask')
        features = extractor.execute(scan, voi) # Do i do voxel-wise or segmentation based feature extraction?? 
        all_disc_features.append(features)

    # obtaining feature names
    valid_features = list(sorted(filter(lambda x : x.startswith('original_'), all_disc_features[0])))
    valid_features = np.asarray(valid_features)

    rad_feature = np.zeros((valid_features.shape[0], len(all_disc_features)))

    # Loop through all disc feature dicts 
    for i in range(len(all_disc_features)):
        feature_vals = np.array([]) # = []
        # For every valid feature append its value in the current disc feature dict (features)
        # to the ndarray feature_vals
        for feature_name in valid_features:
            feature_vals = np.append(feature_vals, all_disc_features[i][feature_name])

        rad_feature[:, i] = feature_vals

    return rad_feature


# implementing feature extraction function to extract features of disc segments 
extractor = featureextractor.RadiomicsFeatureExtractor()

test_subject = ###<insert patient list>###
# test_subject = ['S204','S216'] <- example 

subject_feat_dict = {}
for subject in test_subject: 
    # data_path = f'/Users/bukowskin/Documents/school_repos/data_practicum/spine_segment_2023/{subject}/' <- personal dir
    data_path = f'/data/cs3310/MuftulerLab1/data/spine/{test_subject}/' # path setup for Rosie
    scan_path = data_path + f'{subject.lower()}_T2.nii.gz'
    disc_masks = [(data_path + 'vois/L5S1_pred.nii'),
                  (data_path + 'vois/L5S1_pred.nii'),
                  (data_path + 'vois/L1L2_pred.nii'),
                  (data_path + 'vois/L2L3_pred.nii'),
                  (data_path + 'vois/L3L4_pred.nii'),
                  (data_path + 'vois/L4L5_pred.nii')]
    rad_feature = get_disc_rad_features(extractor, scan_path, disc_masks)
    subject_feat_dict[subject] = rad_feature



